//
//  RMUniversalAlert+Error.m
//
//  Created by Mukhail Mukminov on 13.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import "UIViewController+ErrorAlert.h"

@implementation UIViewController (Error)

- (void)showErrorAlertWithMessage:(NSString *)message
{
    [RMUniversalAlert showAlertInViewController:self
                                      withTitle:NSLocalizedString(@"Error", nil)
                                        message:message
                              cancelButtonTitle:NSLocalizedString(@"Close", nil)
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil
                                       tapBlock:nil];
}

- (void)showAlertWithError:(NSError *)error
{
    [self showErrorAlertWithMessage:error.localizedDescription];
}

@end

//
//  RMUniversalAlert+Error.h
//
//  Created by Mukhail Mukminov on 13.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RMUniversalAlert/RMUniversalAlert.h>

@interface UIViewController (ErrorAlert)

- (void)showErrorAlertWithMessage:(NSString *)message;
- (void)showAlertWithError:(NSError *)error;

@end

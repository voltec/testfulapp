//
//  UIViewController+MBProgressHUD.h
//
//  Created by Mukhail Mukminov on 13.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface UIViewController (MBProgressHUD)

- (void)showHud;
- (void)hideHud;

@end

//
//  UIViewController+MBProgressHUD.m
//
//  Created by Mukhail Mukminov on 13.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import "UIViewController+MBProgressHUD.h"

@implementation UIViewController (MBProgressHUD)

- (void)showHud
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)hideHud
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

@end

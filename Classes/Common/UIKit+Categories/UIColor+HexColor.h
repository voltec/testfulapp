//
//  UIColor+HexColor.h
//
//  Created by Mukhail Mukminov on 13.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexColor)

+ (UIColor *)colorFromHexValue:(NSInteger)rgbValue;

@end

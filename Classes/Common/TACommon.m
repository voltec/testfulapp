//
//  TACommon.m
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import "TACommon.h"

#import "UIColor+HexColor.h"

BOOL isIOS9()
{
    NSOperatingSystemVersion version = [[NSProcessInfo processInfo] operatingSystemVersion];
    
    return version.majorVersion == 9;
}

void customize()
{
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorFromHexValue:0xff7d67]}];
}
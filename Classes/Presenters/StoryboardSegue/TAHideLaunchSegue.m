//
//  TAHideLaunchSegue.m
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import "TAHideLaunchSegue.h"

#import "TALaunchVC.h"

@implementation TAHideLaunchSegue

- (UIImage *)screenshotWithView:(UIView *)view
{
    CGRect rect = view.frame;
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context=UIGraphicsGetCurrentContext();
    [view.layer renderInContext:context];
    
    UIImage *image=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)perform {
    
    TALaunchVC *source = self.sourceViewController;
    UIImage *image = [self screenshotWithView:source.view];
    UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
    
    UIWindow *window = source.view.window;
    
    UIViewController *destination = self.destinationViewController;
    
    [window setRootViewController:destination];
    [window addSubview:imageview];
    
    destination.view.frame = CGRectMake(0, 0, window.frame.size.width*0.7, window.frame.size.height*0.7);
    destination.view.center = CGPointMake(window.frame.size.width/2., window.frame.size.height/2.);
    
    [UIView animateWithDuration:0.5
                     animations:^()
     {
         imageview.frame = CGRectMake(-source.view.frame.size.width,
                                        -source.view.frame.size.height,
                                        source.view.frame.size.width*3,
                                      source.view.frame.size.height*3);
         destination.view.frame = CGRectMake(0, 0, window.frame.size.width, window.frame.size.height);
         imageview.alpha = 0;
     }
                     completion:^(BOOL finished)
     {
         [imageview removeFromSuperview];
     }];
}

@end

//
//  TALaunchScreen.h
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TALaunchVC : UIViewController

@property (nonatomic,weak) IBOutlet NSLayoutConstraint *imageWidth;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *imageHeight;

@end

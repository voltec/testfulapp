//
//  TAContactListVC.m
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import "TAContactListVC.h"

#import "TAContactListDataSource.h"
#import "TAContactListInteractor.h"

@interface TAContactListVC ()

@property (nonatomic,strong) TAContactListInteractor *contactListInteractor;
@property (nonatomic,strong) TAContactListDataSource *dataSource;

@end

@implementation TAContactListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Chats", nil);
    
    self.dataSource = [TAContactListDataSource new];
    self.tableView.dataSource = self.dataSource;
    self.tableView.delegate = self.dataSource;
    
    if (isIOS9())
        self.tableView.cellLayoutMarginsFollowReadableWidth = NO;
    
    self.contactListInteractor = [TAContactListInteractor new];
    
    __weak typeof(self) weakSelf = self;
    [self showHud];
    [self.contactListInteractor getContactListWithCompletion:^(NSError *error) {
        [weakSelf hideHud];
        
        if (error)
            [weakSelf showAlertWithError:error];
        else
        {
            weakSelf.dataSource.contactList = weakSelf.contactListInteractor.contacts;
            [weakSelf.tableView reloadData];
        }
    }];
}

@end

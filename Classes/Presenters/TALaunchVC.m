//
//  TALaunchScreen.m
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import "TALaunchVC.h"

@interface TALaunchVC ()

@property (nonatomic,assign) NSTimeInterval duration;
@property (nonatomic,weak) IBOutlet UIImageView *animationView;

@end

@implementation TALaunchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.duration = 0.5;
    [self initAnimation];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.animationView.animationDuration = self.duration;
    [self.animationView startAnimating];
    [NSTimer scheduledTimerWithTimeInterval:self.duration
                                     target:self
                                   selector:@selector(complete)
                                   userInfo:nil
                                    repeats:NO];
}

- (void)complete
{
    [self.animationView stopAnimating];
    self.animationView.animationImages = nil;
    self.animationView.image = [UIImage imageNamed:@"act1"];
    [self performSegueWithIdentifier:@"HideLoading" sender:self];
}

- (void)initAnimation
{
    NSMutableArray *images = [NSMutableArray new];
    for (int i = 1; i <= 6; ++i) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"act%d", i]];
        if (image)
            [images addObject:image];
    }
    
    self.animationView.animationImages = images;
}

@end

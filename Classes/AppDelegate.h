//
//  AppDelegate.h
//  Qwintry
//
//  Created by Mukhail Mukminov on 13.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


//
//  TAContact.h
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TAContact : NSObject

@property (nonatomic,strong) NSString *avatarURL;
@property (nonatomic,strong) NSString *fullname;
@property (nonatomic,strong) NSNumber *identifier;
@property (nonatomic,strong) NSString *message;
@property (nonatomic,strong) NSDate *sendingDate;
@property (nonatomic,strong) NSNumber *unreadMessagesCount;

@end

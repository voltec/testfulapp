//
//  TAContactDataSource.h
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TAContactListInteractor : NSObject

@property (nonatomic,strong) NSArray *contacts;

- (void)getContactListWithCompletion:(void(^)(NSError *error))completion;

@end

//
//  TAContactDataSource.m
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import "TAContactListInteractor.h"
#import "TAServerApiClient.h"
#import "TAGetContactListRequest.h"

@implementation TAContactListInteractor

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)getContactListWithCompletion:(void(^)(NSError *error))completion
{
    TAGetContactListRequest *request = [TAGetContactListRequest new];
    __weak typeof(self) weakSelf = self;
    
    [request setCompletion:^(NSArray *contacts, NSError *error)
    {
        if (error == nil)
        {
            weakSelf.contacts = contacts;
        }
        
        if (completion)
        {
            completion(error);
        }
    }];
    
    [[TAServerApiClient sharedClient] sendGetRequest:request];
}

@end

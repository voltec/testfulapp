//
//  TAContactListDataSource.m
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import "TAContactListDataSource.h"
#import "TAChatCell.h"

@implementation TAContactListDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.contactList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TAContact *contact = self.contactList[indexPath.row];
    
    TAChatCell *cell = [tableView dequeueReusableCellWithIdentifier:TAChatCellIdentifier];
    
    cell.contact = contact;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TAContact *contact = self.contactList[indexPath.row];
    return [TAChatCell heightCellForContact:contact];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end

//
//  TAContactListDataSource.h
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAContactListInteractor.h"

@interface TAContactListDataSource : NSObject <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) NSArray *contactList;

@end

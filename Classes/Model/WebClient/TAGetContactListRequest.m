//
//  TAGetContactListRequest.m
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import "TAGetContactListRequest.h"

#import "TAContact.h"

#import <DCKeyValueObjectMapping/DCKeyValueObjectMapping.h>

@implementation TAGetContactListRequest

- (id)parseResponse:(NSArray *)response
{
    DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[TAContact class]];
    
    NSArray *items = [parser parseArray:response];
    
    return items;
}

@end

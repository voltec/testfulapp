//
//  TAServerApiClient.m
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import "TAServerApiClient.h"

@implementation TAServerApiClient

+ (instancetype)sharedClient
{
    static TAServerApiClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[TAServerApiClient alloc] init];
        //client settings
    });
    
    return _sharedClient;
}

- (NSString *)localizedErrorTextWithCode:(NSInteger)code
{
    //генерация текста ошибки в зависимости от кода
    return nil;
}

- (NSError *)parseError:(NSDictionary *)response
{
    NSError *error = nil;
    
    //parsing error
    
    return error;
}

- (void)sendGetRequest:(TARequest *)request
{
    NSLog(@"Request: %@", request.methodPath);
    NSLog(@"Parameters: %@", request.parameters);
    [self GET:request.methodPath
   parameters:request.parameters
      success:^(id  _Nonnull responseObject)
     {
         NSError *error = [self parseError:responseObject];
         id parsed = nil;
         
         if (error == nil) {
             parsed = [request parseResponse:responseObject];
             NSLog(@"Response: success");
         }
         if (request.completion)
             request.completion(parsed, error);
     }
      failure:^(NSError * _Nonnull error)
     {
         if (request.completion)
             request.completion(nil, error);
     }];
}

#pragma REST requests: need override!

- (void)performAsync:(void (^)(void))block
{
    dispatch_queue_t queue = dispatch_queue_create("simple async queue", DISPATCH_QUEUE_CONCURRENT);
    dispatch_async(queue, ^{
        @autoreleasepool {
            block();
        }
    });
}

- (void)GET:(NSString *)URLString
 parameters:(id)parameters
    success:(void (^)(id responseObject))success
    failure:(void (^)(NSError *error))failure
{
    [self performAsync:^{
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
        NSArray *values = [[NSArray alloc] initWithContentsOfFile:plistPath];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success) {
                success(values);
            }
        });
    }];
}

@end

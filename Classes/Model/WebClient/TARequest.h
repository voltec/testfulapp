//
//  TARequest.h
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import <Foundation/Foundation.h>

#define nilOrJSONObjectForKey(JSON_, KEY_) [[JSON_ objectForKey:KEY_] isKindOfClass:[NSNull class]] ? nil : [JSON_ objectForKey:KEY_]

typedef void(^TARequestHandle)(id response, NSError *error);

@interface TARequest : NSObject

@property (nonatomic,strong) NSString *methodPath;// example: api-rest/v2/user/login
@property (nonatomic,strong) NSDictionary *parameters;// example {@"id" : @"666"}
@property (nonatomic,copy) TARequestHandle completion;

- (id)parseResponse:(id)response;

@end

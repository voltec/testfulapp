//
//  TAServerApiClient.h
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TARequest.h"

@interface TAServerApiClient : NSObject

+ (instancetype)sharedClient;

- (void)sendGetRequest:(TARequest *)request;

@end

//
//  TAChatCell.h
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAContact.h"

extern NSString * const TAChatCellIdentifier;

@interface TAChatCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIImageView *contactImageView;
@property (nonatomic,weak) IBOutlet UILabel *nameLabel;
@property (nonatomic,weak) IBOutlet UILabel *dateLabel;
@property (nonatomic,weak) IBOutlet UILabel *messageLabel;
@property (nonatomic,weak) IBOutlet UILabel *messageCountLabel;

@property (nonatomic,strong) TAContact *contact;

+ (CGFloat)heightCellForContact:(TAContact *)contact;

@end

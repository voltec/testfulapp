//
//  TAChatCell.m
//  TestfulApp
//
//  Created by Mukhail Mukminov on 16.11.15.
//  Copyright © 2015 Mukhail Mukminov. All rights reserved.
//

#import "TAChatCell.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import <Smartling.i18n/SLLocalization.h>

#import "NSDate+ConvertToString.h"
#import "UIColor+HexColor.h"

NSString * const TAChatCellIdentifier = @"TAChatCellIdentifier";

CGFloat const minimumHeight = 112;
CGFloat const messageCountHeight = 33;

@interface TAChatCell ()

@property (nonatomic,weak) IBOutlet NSLayoutConstraint *heightConstraint;

@end

@implementation TAChatCell

+ (CGFloat)heightCellForContact:(TAContact *)contact
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat heightMessage = [contact.message boundingRectWithSize:CGSizeMake(screenWidth-88-8, CGFLOAT_MAX)
                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                       attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                                                          context:nil].size.height;
    
    if (contact.unreadMessagesCount.integerValue == 0) {
        heightMessage -= messageCountHeight;
    }
    
    return minimumHeight + heightMessage;
}

- (void)awakeFromNib {
    // Initialization code
    
    self.nameLabel.font = [UIFont boldSystemFontOfSize:14];
    self.dateLabel.font = [UIFont systemFontOfSize:14];
    self.dateLabel.textColor = [UIColor colorFromHexValue:0xaaaaaa];
    self.messageLabel.font = [UIFont systemFontOfSize:14];
    self.messageCountLabel.font = [UIFont systemFontOfSize:14];
    self.messageCountLabel.textColor = [UIColor colorFromHexValue:0xff7d67];
    
    self.contactImageView.layer.cornerRadius = self.contactImageView.frame.size.width / 2.;
    self.contactImageView.layer.masksToBounds = YES;
}

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = self.contactImageView.bounds;
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)setContact:(TAContact *)contact
{
    _contact = contact;
    
    NSURL *url = [NSURL URLWithString:contact.avatarURL];
    [self.contactImageView sd_setImageWithURL:url
                             placeholderImage:[self imageFromColor:[UIColor colorWithRed:200/255.
                                                                                   green:200/255.
                                                                                    blue:200/255.
                                                                                   alpha:1]]];
    self.nameLabel.text = contact.fullname;
    self.dateLabel.text = [contact.sendingDate stringWithFormat:@"hh:mm a"];
    self.messageLabel.text = contact.message;
    
    if (contact.unreadMessagesCount.integerValue > 0)
    {
        NSString *format = SLPluralizedString(@"+%d message", contact.unreadMessagesCount.integerValue, @"message");
        self.messageCountLabel.text = [NSString stringWithFormat:format, contact.unreadMessagesCount.integerValue];
        self.heightConstraint.constant = messageCountHeight;
    }
    else
    {
        self.messageCountLabel.text = nil;
        self.heightConstraint.constant = 0;
    }
    
    [self setNeedsDisplay];
}

@end
